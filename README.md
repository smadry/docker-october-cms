# Docker October CMS

A basic docker environment for October CMS.

### Get Started

After clone this repository run the follow commands:

```sh
$ cd docker-october-cms
$ cp .envs/env.local .
$ mv env.local .env
$ docker-compose up
$ docker-compose exec phpfpm  bash
$ composer create-project october/october .
$ composer update
$ php artisan october:install
```
Official October Documentation: https://octobercms.com/docs/cms/themes

License
----

MIT